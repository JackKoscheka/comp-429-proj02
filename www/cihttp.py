
import socket, logging, threading, json, codecs, re, time
from time import gmtime

# Comment out the line below to not print the INFO messages
logging.basicConfig(level=logging.INFO)


class HttpRequest():
    def __init__(self, requeststr):
        self.rstr = requeststr
        self.rjson = {}
        self.parse_string()


    def add_request_line(self, request_line):

        # creates and empty dictionary for the request_line
        request_dict = {}

        # adds method to the request_dict
        request_dict['method'] = request_line[0];

        # adds URI to the request_dict
        request_dict['URI'] = request_line[1];

        # adds headers to the request_dict
        request_dict['headers'] = request_line[2];

        # adds request_dict to json object
        self.rjson['request-line'] = request_dict;


    def add_header_list(self, header_lines):

        # creates an empty list for the headers
        header_list = []

        # sets index to 1 to ignore request_line
        index = 1

        # gets the length of the list header_lines
        length = len(header_lines)

        # goes through all the header_lines (ignoring the request_line)
        while index < length:

            # gets the current header_line
            header_line = header_lines[index]

            # splits the current header line into key value pair
            key_value = header_line.split(": ")

            # adds the key value pair to header_list
            header_list.append({ key_value[0] : key_value[1] })

            # increments index
            index += 1

        # adds header_list to json object
        self.rjson['headers'] = header_list


    def add_header(self, header_lines):

        # adds header request line to the json object
        self.add_request_line(header_lines[0].split(" "))

        # adds header list to the json object
        self.add_header_list(header_lines)


    def parse_string(self):

        # splits the read string into a head and body section
        split = self.rstr.split("\r\n\r\n")

        # sets the json object to be an empty dictionary
        self.rjson = {}

        # checks if their is any header data
        if len(split) > 0:

            # adds header data to json object
            self.add_header(split[0].split("\r\n"))

            # checks for body data
            if len(split) > 1 and self.rjson['request-line']['method'] != "HEAD":

                # adds body data to json object
                self.rjson['body'] = split[1]

        print(self.rstr)


    def display_request(self):
        print(self.rjson)



class ClientThread(threading.Thread):
    def __init__(self, address, socket):
        threading.Thread.__init__(self)
        self.csock = socket
        logging.info('New connection added.')

    def HEAD_response(self, status, message):

        # adds http version number, status message, and server attribute to header message
        response_message = "HTTP/1.1 " + status + "\r\nServer: cihttpd"

        # adds Content-length attribute to header message
        response_message += "\r\nContent-length: " + str(len(message))

        # adds Last-modified attribute to header message
        response_message += "\r\nLast-modified: " + str(time.gmtime())

        # ends header section and returns
        return response_message + "\r\n\r\n"

    def GET_response(self, status, message):

        # returns head response with message attached
        return self.HEAD_response(status, message) + message

    def POST_response(self, status, request_json):

        # form submission successful!

        # gets the body split into parts based on request_json
        body = request_json['body'].replace("+", " ").split("&")

        # checks if body contains at least 2 elements for name and course
        if len(body) >= 2:

            # starts html message
            message = "<html><body>"

            # adds title and css styles to message header
            message += "<head><title>Form Submission</title><link rel=\"stylesheet\" href=\"form.css\"></head>"

            # adds text indicating successfull submission
            message += "<h1>Form submission successful!</h1>"

            # adds name to html message
            message += "<p>Thank you for your submission " + body[0].split("=")[1]

            # adds course to html message
            message += " from " + body[1].split("=")[1] + "</p>"

            # ends html message
            message += "</body></html>"

        # body does not provide sufficient information
        else:

            # sets message to display html indicating an invalid response
            message = "<html><body><h1>Invalid Response</h1></body></html>"

        # returns head response with message attached
        return self.HEAD_response(status, message) + message

    def get_response(self, request_json, file_name):

        # tries to read the file
        try:

            # sets the status to ok
            status = "200 OK"

            # sets the message to the read file contents
            message = codecs.open(file_name, "r", "utf-8").read()

        # could not find and read file
        except:

            # sets status to error
            status = "404 File not found"

            # sets message to error
            message = "<html><body><h1>404 File not found</h1></body></html>"

        # gets the header_request from request_json
        request_type = request_json['request-line']['method']

        # returns the response based on request_type
        return {

            # returns head response
            'HEAD' : self.HEAD_response(status, message),

            # returns get repsonse
            'GET' : self.GET_response(status, message),

            # returns post response
            'POST' : self.POST_response(status, request_json),

        # gets the response for the request
        }.get(request_type, 0)


    def run(self):
        # exchange messages
        request = self.csock.recv(1024)
        req = request.decode('utf-8')
        logging.info('Recieved a request from client: ' + req)
        httpreq = HttpRequest(req)
        httpreq.display_request()

        # gets the requested file from the http request
        file_name = httpreq.rjson['request-line']['URI']

        # checks for a / at the start of URI
        if file_name[0] == '/':

            # gets the subtring after / for file path
            file_name = file_name[1:]

        # converts the file_name to lower case
        file_name = file_name.lower()

        # checks if the file has an extension
        if not "." in file_name:

            # appends .html to file_name for default extension
            file_name += ".html"

        # send a response
        self.csock.send(bytes(self.get_response(httpreq.rjson, file_name), 'utf-8'))

        # disconnect client
        self.csock.close()
        logging.info('Disconnect client.')


def server():
    logging.info('Starting cihttpd...')

    # start serving (listening for clients)
    port = 9001
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('localhost', port))

    while True:
        sock.listen(1)
        logging.info('Server is listening on port ' + str(port))

        # client has connected
        sc,sockname = sock.accept()
        logging.info('Accepted connection.')
        t = ClientThread(sockname, sc)
        t.start()


server()

